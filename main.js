// const autoriz = {
//     email: 'ols27@ukr.net',
//     password: '11111111'
// };

const autoriz = {
    email: '',
    password: ''
};

let myToken='';
let headerRequest = {
    Authorization: myToken,
    'Content-Type': 'application/json',
};

class API{
    constructor(urlStr, methodStr, headersObj, dataObj){
        this.urlStr=urlStr;
        this.dataObj=dataObj;
        this.methodStr=methodStr;
        this.headersObj=headersObj;
    };
    // Значения по умолчанию обозначены знаком *
    request(){
        return fetch(this.urlStr, {
            method: this.methodStr, // *GET, POST, PUT, DELETE, etc.
            // mode: 'cors', // no-cors, cors, *same-origin
            // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: 'same-origin', // include, *same-origin, omit
            headers: this.headersObj,
            // redirect: 'follow', // manual, *follow, error
            // referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(this.dataObj), // тип данных в body должен соответвовать значению заголовка "Content-Type"
        })
            .then(response =>response.json()) // парсит JSON ответ в Javascript объект
    }
}



// =====================================================================================
let headerModalReasonVisit = document.getElementsByClassName('header-modalReasonVisit')[0];
let headerAddNewVisit = document.getElementsByClassName('header-addNewVisit')[0];
let buttonExitVisit = document.getElementsByClassName('buttonExitVisit')[0];
buttonExitVisit.onclick = function() {headerModalReasonVisit.style.display = 'none'};
let schedule = document.getElementsByClassName('schedule')[0];
console.log(schedule);
schedule.addEventListener('click',(event)=>{
    headerModalReasonVisit.onclick = function(event) {event.stopPropagation()}
    if (event.target===headerAddNewVisit || event.currentTarget===headerModalReasonVisit){
        headerModalReasonVisit.style.display = 'block'
    } else headerModalReasonVisit.style.display = 'none'
    console.log(event.target);
})
let inputParamVisit = document.querySelector('.inputParamVisit');
let buttonCreateVisit = document.querySelector('.buttonCreateVisit');
let serviceDesc = document.querySelector('.serviceDesc');
let serviceDescEmpty = document.querySelector('.serviceDescEmpty');
let pacientField={
    Cardiologist:['Фамилия','Имя','Отчество','Возвраст','Цель визита','Обычное давление','Индекс масы тела','Перенесённые заболевания ССС','Комментарии'],
    Dentist:['Фамилия','Имя','Отчество','Цель визита','Дата последнего визита','Комментарии'],
    Therapist:['Фамилия','Имя','Отчество','Возвраст','Цель визита','Комментарии'],
};
class Visit{
    constructor(obj){
        this.pacientFieldObj=obj
    }
}
class CreateFieldDoctorSpecialty extends Visit{
    constructor(){
        super(pacientField);
        this.doctorSpecial=document.getElementById('header-modalDoctorSpecialty').value;
        this.containerProfiled = document.querySelector('.containerProfiled');
        this.pacientProfileFieldsArr =[...this.containerProfiled.children];
    }
    createInput(placeholder) {
        this.createInputElem = document.createElement('input');
        this.containerProfiled.appendChild(this.createInputElem);
        this.createInputElem.placeholder=placeholder;
        this.createInputElem.style.width = '70%';
        this.createInputElem.style.height = '30%';
    }
    removeInputs(){
        this.pacientProfileFieldsArr.forEach((e)=>{
            e.remove();
        });
    }
    createFieldInModalWindow(){
        switch (this.doctorSpecial){
            case "Cardiologist":
                this.removeInputs();
                this.pacientFieldObj.Cardiologist.forEach((element)=>{
                    this.createInput(element);
                });
                break;
            case "Dentist":
                this.removeInputs();
                this.pacientFieldObj.Dentist.forEach((element)=>{
                    this.createInput(element);
                });
                break;
            case "Therapist":
                this.removeInputs();
                this.pacientFieldObj.Therapist.forEach((element)=>{
                    this.createInput(element);
                });
        }
    }
}
inputParamVisit.onclick = function getDoctorData() {
    let createInput = new CreateFieldDoctorSpecialty();
    createInput.createFieldInModalWindow();
    buttonCreateVisit.removeAttribute('disabled')
};

let ObjForRender = {};
    buttonCreateVisit.addEventListener('click',(event)=>{
    event.preventDefault();
    buttonCreateVisit.setAttribute('disabled','true')
    let renderOnServiseDesc = new Render;
    renderOnServiseDesc.createObjForRender();
    sendObjOnServ();
    renderOnServiseDesc.createCard();

    console.log(autoriz.email);
    console.log(autoriz.password);
});

class Render extends CreateFieldDoctorSpecialty{
    createObjForRender(){
        let containerProfiled = document.querySelector('.containerProfiled').children;
        [...containerProfiled].forEach((element)=>{
            ObjForRender[element.placeholder] = element.value;
            // console.log(ObjForRender);
        });
    }
    createCard(){
        let cardDiv = document.createElement('div');
        serviceDesc.appendChild(cardDiv);
        let x = document.createElement('button');
        x.innerText = 'x';
        x.style.cssText=`align-self: flex-end`;
        cardDiv.appendChild(x);
        x.onmousedown=function(event){
            let idObject = +event.target.parentNode.lastElementChild.textContent;
            deleteObjFromServ(idObject);
            cardDiv.style.display = 'none';
        };
        cardDiv.classList = 'flexClass';
        serviceDescEmpty.remove();
        cardDiv.style.flexDirection = 'column';
        cardDiv.style.margin = '0 auto';
        cardDiv.style.border = '1px solid #9c9c9c';
        cardDiv.style.height = '100px';
        cardDiv.style.overflow = 'hidden';
        cardDiv.style.margin = '10px';
        for(let key in ObjForRender){
            let infoField = document.createElement('spam');
            cardDiv.appendChild(infoField);
            infoField.innerText=ObjForRender[key]
        }
        cardDiv.onmousedown = function(e) {
            // console.log(cardDiv);
            cardDiv.style.position = 'absolute';
            cardDiv.style.zIndex = 1000;
            moveAt(e);
            function moveAt(e) {
                cardDiv.style.left = e.pageX - cardDiv.offsetWidth / 2 + 'px';
                cardDiv.style.top = e.pageY - cardDiv.offsetHeight / 2 + 'px';
            }
            document.onmousemove = function(e) {
                moveAt(e);
            };
            cardDiv.onmouseup = function() {
                document.onmousemove = null;
                cardDiv.onmouseup = null;
            }
        }
    }
}

let sendObjOnServ = ()=>{
    headerRequest.Authorization=myToken;
    let sendObj = new API('http://cards.danit.com.ua/cards','POST', headerRequest, ObjForRender);
    sendObj.request()
        .then(data => {
            console.log(data);
        }) // JSON-строка полученная после вызова `response.json()`
        .catch(error => console.error(error));
};

let getObjFromServ = ()=> {
    let getObj = new API('http://cards.danit.com.ua/cards', 'GET', headerRequest);
    headerRequest.Authorization = myToken;
    getObj.request()
        .then(data => {
            console.log(data);
            data.forEach((elem)=>{
                let displayAllCards = new Render;
                ObjForRender = elem;
                displayAllCards.createCard()
            })
        }) // JSON-строка полученная после вызова `response.json()`
        .catch(error => console.error(error));
};

let deleteObjFromServ = (id)=> {
    let Url = 'http://cards.danit.com.ua/cards/'+id;
    let deleteObj = new API(Url,'DELETE', headerRequest);
    headerRequest.Authorization = myToken;
    deleteObj.request()
        .then(data => {
            if(data.status!=='Success'){
                throw ('Delete status not Success')
            }
        }) // JSON-строка полученная после вызова `response.json()`
    // .catch(error => console.error(error));
};


let registerForm = document.getElementsByClassName('registerForm')[0];
registerForm.onsubmit = function (e) {
    e.preventDefault();
    const autoriz = Object.fromEntries(new FormData(registerForm));
    console.log(autoriz);

    let passAvtoriz = new API('http://cards.danit.com.ua/login','POST',headerRequest, autoriz);
    passAvtoriz.request()
        .then(data => {
            const {token} = data;
            myToken= `Bearer ${token}`;
            console.log(data);
            getObjFromServ();
        }) // JSON-строка полученная после вызова `response.json()`
        .catch(error => console.error(error,'error autorization')
        );


};

let submitButtonRegistr = document.getElementsByClassName('submitButtonRegistr')[0];

    submitButtonRegistr.addEventListener('click', ()=>{
    document.getElementsByClassName('register')[0].style.display='none';





    })